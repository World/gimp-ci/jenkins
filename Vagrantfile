# -*- mode: ruby -*-
# vi: set ft=ruby :

# Execute shell script before running vagrant.  This script will run on every
# vagrant command.
system('./scripts/vagrant-up.sh')

Vagrant.configure("2") do |config|
  # https://docs.vagrantup.com.
  config.vm.box = "centos/7"
  config.vm.network "forwarded_port", guest: 8080, host: 8080, host_ip: "127.0.0.1"
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "4096"
  end

  config.vm.provision "shell", inline: <<-SHELL
    set -e

    # install EPEL repo
    rpm -qa | grep -- epel-release || (
      yum makecache fast
      yum install -y epel-release
    )


    # install IUS repo
    rpm -qa | grep ius-release || (
      [ -r /tmp/ius.asc ] || curl -fLo /tmp/ius.asc https://dl.iuscommunity.org/pub/ius/IUS-COMMUNITY-GPG-KEY
      echo '688852e2dba88a3836392adfc5a69a1f46863b78bb6ba54774a50fdecee7e38e  /tmp/ius.asc' | sha256sum -c
      rpm --import /tmp/ius.asc
      [ -r /tmp/ius.rpm ] || curl -fLo /tmp/ius.rpm https://centos7.iuscommunity.org/ius-release.rpm
      rpm -K /tmp/ius.rpm
      yum localinstall -y /tmp/ius.rpm
    )


    # install Docker repo
    [ -f /etc/yum.repos.d/docker-ce.repo ] || yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo


    # install packages
    rpm -qa | grep -- docker-ce || (
      yum makecache
      yum install -y yum-utils device-mapper-persistent-data lvm2 git2u docker-ce
      yum install -y vim bind-utils net-tools nc ntpdate
      systemctl start ntpdate
      systemctl enable ntpdate
      systemctl enable docker
      systemctl start docker
    )

    # install Java
    yum install -y java-1.8.0-openjdk-devel.x86_64
    #install Jenkins
    rpm -i /vagrant/build/distributions/*.rpm
    if ! groups jenkins | grep docker; then
      usermod -a -G docker jenkins
    fi
    #pull down GIMP development environment
    docker pull gimp/gimp:latest

    # patch the Docker image to match the UID and GID used by Jenkins
    JENKINS_UID="$(su -s /bin/bash -c 'id -u' - jenkins)"
    JENKINS_GID="$(su -s /bin/bash -c 'id -g' - jenkins)"
    DOCKER_UID="$(docker run --rm gimp/gimp id -u)"
    DOCKER_GID="$(docker run --rm gimp/gimp id -g)"
    cat > /tmp/Dockerfile <<EOF
FROM gimp/gimp:latest
USER root
RUN \
sed -i -- 's/:${DOCKER_UID}:${DOCKER_GID}:/:${JENKINS_UID}:${JENKINS_GID}:/' /etc/passwd && \
sed -i -- 's/:${DOCKER_GID}:/:${JENKINS_GID}:/' /etc/group && \
chown -R jenkins:jenkins /data /home/jenkins
USER jenkins
EOF
    docker build -f /tmp/Dockerfile -t gimp/gimp:latest /tmp

    #start the Jenkins daemon
    /etc/init.d/jenkins start
  SHELL
end
