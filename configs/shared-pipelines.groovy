pipeline_shared_libraries = [
    'GIMP Pipeline Library': [
        'defaultVersion': 'master',
        'implicit': true,
        'allowVersionOverride': false,
        'includeInChangesets': true,
        'scm': [
            'remote': 'https://gitlab.gnome.org/World/gimp-ci/jenkins-dsl.git'
        ]
    ]
]

