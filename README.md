# New to GIMP CI?

See our [centralized documentation][documentation].

This project contains:

- Infrastructure as Code: see [Jenkins Software
  Packages](#jenkins-software-packages).
- Configuration as Code: see [Configuring Jenkins](#configuring-jenkins).

# Cloning this project

This project includes submodules and requires recursive cloning.

    git clone --recursive https://gitlab.gnome.org/World/gimp-ci/jenkins.git

If you've already cloned this project then run the following to fetch
submodules.

    git submodule update --init --recursive

# Provision build.gimp.org locally

This repository allows a developer to provision the Jenkins build server used to
build GIMP locally.  There's two ways to provision it: with [Vagrant][vagrant] +
[VirtualBox][vbox] and without.

### Requirements

If provisioning with Vagrant, then there requirements for any operating system
are that [Vagrant][vagrant] and [VirtualBox][vbox] be installed.

If starting Jenkins directly without using VirtualBox or Vagrant, then the
following is required:

- A bash shell.
- Docker CE installed.
- The local user needs to be able to run `docker` commands. e.g. `docker
  images`.
- The UID/GID of the user needs to be `1000:1000`

### Provisioning Jenkins

With Vagrant + VirtualBox:

    export VAGRANT_JENKINS=1
    vagrant up
    ./jenkins_bootstrap.sh

Alternatively, provision Jenkins directly (without Vagrant or VirtualBox):

    ./jenkins_bootstrap.sh

# Jenkins Software Packages

Linux DEB and RPM system packages for [build.gimp.org][build] Jenkins.
Downloads are available in project [`tags` hosted by GNOME GitLab][tags].

### Why?

This is to support an immutable infrastructure model of Jenkins for the GIMP CI
project.  When installing Jenkins, it typically only installs the latest version
of the WAR and the latest version of all of the plugins.  This makes reliably
reproducing issues difficult.  Jenkins publishes artifacts to a [Maven
repository](https://repo.jenkins-ci.org/).  This project is designed to download
the exact version of the Jenkins WAR as well as exact versions of all of the
Jenkins plugins.  This makes configuring and testing upgrades more reliable.

### Building Software Packages

Build release packages automatically:

    ./jenkins-bootstrap-shared/scripts/buildRelease.sh

Learn more at [jenkins-bootstrap-shared][jenkins-bootstrap] project.  Release
packages will then be available in the `build/distributions/` directory.

### Locally provision Jenkins

Execute the following.

    ./jenkins_bootstrap.sh

Visit `http://localhost:8080/` so see the Jenkins UI.

# Configuring Jenkins

Configuration for Jenkins is stored in the [`configs/`](configs) directory.

If you're provisioning Jenkins locally, then
[`jenkins_bootstrap.sh`](jenkins_bootstrap.sh) will automatically configure
Jenkins during provisioning.

If you're configuring Jenkins from a remote system then the following steps must
be followed.

```bash
export REMOTE_JENKINS=1 JENKINS_USER=samrocketman
export JENKINS_WEB=https://build.gimp.org/ JENKINS_PASSWORD
read -sp Password: JENKINS_PASSWORD
# configure remote Jenkins
./jenkins_bootstrap.sh
```

# License

* [Apache License 2.0](LICENSE)

[build]: https://build.gimp.org/
[documentation]: https://gitlab.gnome.org/World/gimp-ci/documentation
[jenkins-bootstrap]: https://github.com/samrocketman/jenkins-bootstrap-shared
[tags]: https://gitlab.gnome.org/World/gimp-ci/jenkins/tags
[vagrant]: https://www.vagrantup.com/
[vbox]: https://www.virtualbox.org/
